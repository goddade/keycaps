cfn= 30;

flr= 6;
flh= 2;

cbw= 20;
hlr= 1.6;

cuh= 2+flh;
cdh= 5;


module cutUpMod(){
    difference(){
        cube([cbw, cbw, cuh], center=true);
        translate([0, 0, -cuh/2- 0.01])
            cylinder(cuh-1+ 0.01, r=flr-0.1, $fn= flr* cfn);
        translate([0, 0, -cuh/2+ 0.01])
            cylinder(cuh+ 0.02, r=flr-0.1-1, $fn= flr* cfn);
        
        translate([cbw/2- 2*hlr, cbw/2- 2*hlr, 0])
            cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
        translate([-(cbw/2- 2*hlr), cbw/2- 2*hlr, 0])
            cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
        translate([cbw/2- 2*hlr, -(cbw/2- 2*hlr), 0])
            cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
        translate([-(cbw/2- 2*hlr), -(cbw/2- 2*hlr), 0])
            cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
    }
}

//cutUpMod();

module cutDwMod(){
    difference(){
        union(){
            difference(){
                cube([cbw, cbw, cdh], center=true);
                translate([cbw/2- 2*hlr, cbw/2- 2*hlr, 0])
                    cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
                translate([-(cbw/2- 2*hlr), cbw/2- 2*hlr, 0])
                    cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
                translate([cbw/2- 2*hlr, -(cbw/2- 2*hlr), 0])
                    cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
                translate([-(cbw/2- 2*hlr), -(cbw/2- 2*hlr), 0])
                    cylinder(20, r=hlr, center=true, $fn=hlr* cfn);
            }
            cylinder(cdh/2+ 2, r=flr-0.1, $fn= flr* cfn);
        }
        cylinder(20, r=1, center=true, $fn= flr* cfn);
    }
}
cutDwMod();

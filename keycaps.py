from solid import *
from solid.utils import *

DC= 0.01
CFN= 10
BO= 0.1

TH= 1

swcw= 6.5
swcl= 5

swhx= 1.2+ BO* 2
swhy= 4+ BO* 2

cpr= 7.5

flh= 1.5
flr= cpr- th

shh= flh+ th

def hoopMod(rl, rm):
    return rote_extrude(convexity= 10, fn= cfn* rl)

scad_render_to_file(d, "d.scad")


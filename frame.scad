dc= 0.01;
BO= 0.1;
cfn= 10;

swcw= 6.5;
swcl= 5;

swhx= 1.2+ BO*2;
swhy= 4+ BO*2;
swhh= 3.5+ BO;

bh= 3;
ch= 3;
cr= 6;
dth= 1;
//grommet
gth= 0.2;



module frameMod(){
    cube([20, 20, bh], center=true);
    cylinder(1.2+ bh/2, r=cr, $fn= cr*cfn);
    cylinder(ch- dth + bh/2, r=3, $fn= 3*cfn);
    translate([0,0, (bh/2+ ch- dth- gth)/2])
        cube([12, 1, bh/2+ ch- dth- gth], center=true);
    translate([0,0, (bh/2+ ch- dth- gth)/2])
        cube([1, 12, bh/2+ ch- dth- gth], center=true);
}

frameMod();

module frameMod2(){
    cylinder(6, r= 6, $fn= 6* cfn, center=true);
    translate([4, 0, 0])
        cube([8, 12, 6], center=true);
}

//frameMod2();
dc= 0.01;
BO= 0.1;
cfn= 10;

swcw= 6.5;
swcl= 5;

swhx= 1.2+ BO*2;
swhy= 4+ BO*2;
swhh= 3.5+ BO;



flh= 1.5;
flr= 6;

module edgeSupMod(){
    difference(){
        cylinder(0.2, r=flr, center=true, $fn= flr*cfn);
        cylinder(0.2+ 2*dc, r=flr-0.2, center=true, $fn= flr*cfn);
    }
    translate([0, 0, 0.1/2])
    difference(){
        cylinder(0.3, r=flr, center=true, $fn= flr*cfn);
        cylinder(0.3+ 2*dc, r=flr-0.1, center=true, $fn= flr*cfn);
    }
}
edgeSupMod();
dc= 0.01;
BO= 0.1;
cfn= 10;

swcw= 6.5;
swcl= 5;

swhx= 1.2+ BO*2;
swhy= 4+ BO*2;
swhh= 3.5+ BO;



flh= 1.5;
flr= 6;


module swInMod() {
    translate([0, 0, -(swhh+ dc)/2])
    difference(){
        cube([swcl, swcw, swhh+ dc], center=true);
        cube([swhx, swhy, swhh+ 2* dc], center=true);
        cube([swhy*2, swhx, swhh+ 2* dc], center=true);
    }
}
//swInMod();
module downMod(){
    translate([0, 0, flh/2])
        cylinder(flh, r=flr, center=true, $fn= flr*cfn);
    swInMod();

}

downMod();

dc= 0.01;
BO= 0.15;

dr= 6;
tr= 10;
cfn= 10;

cth= 2;
odr= dr+ cth+ 2*BO;

bh= 5;

module downMod(dmr){
    translate([-dmr, -dmr, 0])
        cube([2*dmr, 2*dmr, bh]);
    translate([dmr, 0, 0])
        cylinder(bh, r=dmr, $fn= dmr*cfn);
    translate([-dmr, 0, 0])
        cylinder(bh, r=dmr, $fn= dmr*cfn);
}

module topMod(tmr){
    resize([2*tmr, 2*dr,0.1])
        cylinder(0.1, r=tmr, $fn= tmr*cfn);
}

module cenMod() {
    hull(){
        translate([0, 0, 20])
            topMod(tr-BO);
        downMod(dr-BO);
    }
}

module outMod(){
//    translate([0, 0, 5- dc])
        difference(){
            translate([0, 0, dc/3])
                cube([32, 20, bh*2- dc], center= true);
            downMod(dr+ 1+ BO);
        }
}

//outMod();
//cenMod();
difference(){
    translate([0, 0, bh/2])
        cube([32, 20, bh- dc], center= true);
    downMod(dr+ 1+ BO);
}